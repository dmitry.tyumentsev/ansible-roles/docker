Docker role
=========

Installs [Docker CE](https://docs.docker.com/engine/) on RHEL/CentOS, Debian/Ubuntu or AltLinux servers.

Example Playbook
----------------
```yml
- hosts: docker
  become: true
  roles:
    - docker
```
